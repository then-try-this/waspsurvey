pkill -f mod_wsgi-express
source venv/bin/activate
sleep 2
nohup mod_wsgi-express start-server --port 8894 --url-alias /static ./static waspsurvey/wsgi.py &

